## Lib gazon
Petite bibliothèque permettant de faire bouger des tondeuses sur une pelouse.
### Usage

#### OpenPelouse
Lib-gazon supporte la spécification "OpenPelouse".  
```Java
String input = "5 5"+System.lineSeparator()
		+"1 2 N"+System.lineSeparator()
		+"GAGAGAGAA"+System.lineSeparator()
		+"3 3 E"+System.lineSeparator()
		+"AADAADADDA";
InputStream stream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8.name()));
Pelouse pelouse = new OpenPelouseLoader().loadPelouse(stream);
```

#### Tondre la pelouse
```Java
pelouse.mow();
```

#### Position finale des tondeuses
```
for (Tondeuse tondeuse : pelouse.getTondeuses()) {
			tondeuse.toString(); //1 3 N
			tondeuse.getX(); //1
			tondeuse.getY(); //3
			tondeuse.getOrientation(); //Orientation.NORTH
}
```
