package com.mowitnow.libgazon.io;

import java.io.InputStream;

import com.mowitnow.libgazon.core.Pelouse;
import com.mowitnow.libgazon.io.OpenPelouseLoader.LoadingException;

public interface PelouseLoader {

	/**
	 * Charge une pelouse et les tondeuses qu'elle contient
	 * @param input les données de la pelouse
	 * @return la pelouse chargée
	 * @throws LoadingException si les données n'ont pas pu être lues / comprises
	 */
	public Pelouse loadPelouse(InputStream input) throws LoadingException;
}
