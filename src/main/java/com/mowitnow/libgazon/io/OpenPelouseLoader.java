package com.mowitnow.libgazon.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.mowitnow.libgazon.core.Orientation;
import com.mowitnow.libgazon.core.Pelouse;
import com.mowitnow.libgazon.core.Tondeuse;

/**
 * Supporte la spécification OpenPelouse
 */
public class OpenPelouseLoader implements PelouseLoader {

	private static final Pattern PATTERN_DIMENSIONS = Pattern.compile("(\\d+) (\\d+)");
	private static final Pattern PATTERN_TONDEUSE = Pattern.compile("(\\d+) (\\d+) ([NSEW])");
	private static final Pattern PATTERN_INSTRUCTIONS = Pattern.compile("([DGA]*)");
	
	/**
	 * Charge une pelouse et les tondeuses qu'elle contient
	 * @param input les données de la pelouse au format OpenPelouse. Exemple : <br />
	 * 5 5<br />
	 * 1 2 N<br />
     * GAGAGAGAA<br />
	 * 3 3 E<br />
	 * AADAADADDA
	 * @return la pelouse chargée
	 * @throws LoadingException si les données n'ont pas pu être lues / comprises
	 */
	public Pelouse loadPelouse(InputStream input) throws LoadingException {
		Pelouse pelouse = new Pelouse();
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(input,Charset.forName("UTF-8")));
			
			Matcher matcherDimensions = PATTERN_DIMENSIONS.matcher(reader.readLine());
			matcherDimensions.find();
			pelouse.setWidth(Integer.parseInt(matcherDimensions.group(1)));
			pelouse.setHeight(Integer.parseInt(matcherDimensions.group(2)));
			
			String ligne = null;
			while ((ligne = reader.readLine()) != null) {
				Tondeuse tondeuse = new Tondeuse();
				Matcher matcherTondeuse = PATTERN_TONDEUSE.matcher(ligne);
				Matcher matcherInstructions = PATTERN_INSTRUCTIONS.matcher(reader.readLine());
				matcherTondeuse.find();
				matcherInstructions.find();
				tondeuse.setX(Integer.parseInt(matcherTondeuse.group(1)));
				tondeuse.setY(Integer.parseInt(matcherTondeuse.group(2)));
				tondeuse.setOrientation(Orientation.getOrientationByChar(matcherTondeuse.group(3).charAt(0)));
				tondeuse.setInstructions(matcherInstructions.group(1));
				pelouse.getTondeuses().add(tondeuse);
			}
		}
		catch (IllegalStateException e) {
			throw new LoadingException(e);
		}
		catch (IOException e) {
			throw new LoadingException(e);
		}
		
		return pelouse;
	}
	
	public static class LoadingException extends Exception {
		
		public LoadingException(String message) {
			super(message);
		}
		
		public LoadingException(Exception e) {
			super(e);
		}
	}
}
