package com.mowitnow.libgazon.core;

import java.util.Arrays;
import java.util.List;

public class Tondeuse {

	private static final List<Orientation> ORDRE_ORIENTATIONS = Arrays.asList(Orientation.NORTH, Orientation.EAST, Orientation.SOUTH, Orientation.WEST);
	
	private int x,y;
	private Orientation orientation;
	private String instructions;
	
	public String toString() {
		return x + " " + y + " " + orientation.getOrientation();
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public Orientation getOrientation() {
		return orientation;
	}

	public void setOrientation(Orientation orientation) {
		this.orientation = orientation;
	}

	public String getInstructions() {
		return instructions;
	}

	public void setInstructions(String instructions) {
		this.instructions = instructions;
	}
	
	/**
	 * Enchaine les instructions
	 */
	public void resolveInstructions(Pelouse pelouse) {
		for (int i = 0; i < instructions.length(); i++) {
			char instruction = instructions.charAt(i);
			if (instruction == 'A') {
				moveForward(pelouse.getWidth(),pelouse.getHeight());
			}
			else {
				turn(instruction);
			}
		}
	}
	
	
	/**
	 * Avance dans la direction actuelle
	 * @param maxWidth Les coordonnées du bord droit
	 * @param maxHeight Les coordonnées du bord haut
	 * @return true si le mouvement a bien eu lieu, false sinon
	 */
	public boolean moveForward(int maxWidth, int maxHeight) {
		int nextX = this.x + orientation.getX();
		int nextY = this.y + orientation.getY();
		if (nextX > maxWidth || nextY > maxHeight || nextX < 0 || nextY < 0) {
			return false;
		}
		
		this.x += orientation.getX();
		this.y += orientation.getY();
		return true;
	}
	
	/**
	 * Pivote de 90 degrés
	 * @param instruction D pour aller tourner à droite, gauche sinon
	 */
	public void turn(char instruction) {
		int orientationActuelle = ORDRE_ORIENTATIONS.indexOf(orientation);
		int futureOrientation = Math.floorMod(orientationActuelle + (instruction == 'D' ? 1 : -1), ORDRE_ORIENTATIONS.size());
		setOrientation(ORDRE_ORIENTATIONS.get(futureOrientation));
	}
}
