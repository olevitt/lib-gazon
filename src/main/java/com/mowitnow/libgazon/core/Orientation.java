package com.mowitnow.libgazon.core;

public enum Orientation {



	NORTH('N'), EAST('E'), WEST('W'), SOUTH('S');


	private char orientation;

	Orientation(char orientation) {
		this.orientation = orientation;
	}

	public static Orientation getOrientationByChar(char orientation) {
		for (Orientation candidat : values()) {
			if (candidat.orientation == orientation) {
				return candidat;
			}
		}
		return null;
	}

	public char getOrientation() {
		return orientation;
	}

	public int getX() {
		switch (orientation) {
		case 'N':
			return 0;
		case 'S':
			return 0;
		case 'E':
			return 1;
		case 'W':
			return -1;
		}
		return 0;
	}

	public int getY() {
		switch (orientation) {
		case 'N':
			return 1;
		case 'S':
			return -1;
		case 'E':
			return 0;
		case 'W':
			return 0;
		}
		return 0;
	}

}
