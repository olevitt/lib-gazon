package com.mowitnow.libgazon.core;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Pelouse {

	private List<Tondeuse> tondeuses = new ArrayList<>();
	private int width, height;

	public List<Tondeuse> getTondeuses() {
		return tondeuses;
	}

	public void setTondeuses(List<Tondeuse> tondeuses) {
		this.tondeuses = tondeuses;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}
	
	/**
	 * Active les tondeuses et résoud leurs instructions
	 */
	public void mow() {
		for (Tondeuse tondeuse : tondeuses) {
			tondeuse.resolveInstructions(this);
		}
	}
	
	
}
