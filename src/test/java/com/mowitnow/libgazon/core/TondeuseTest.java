package com.mowitnow.libgazon.core;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TondeuseTest {

	private Tondeuse tondeuse;
	
	@Before 
	public void before() {
		tondeuse = new Tondeuse();
		tondeuse.setX(0);
		tondeuse.setY(0);
		tondeuse.setOrientation(Orientation.EAST);
	}
	
	@Test
	public void turnTest() {
		tondeuse.turn('D');
		Assert.assertEquals(tondeuse.getOrientation(), Orientation.SOUTH);
		tondeuse.turn('D');
		Assert.assertEquals(tondeuse.getOrientation(), Orientation.WEST);
		tondeuse.turn('D');
		Assert.assertEquals(tondeuse.getOrientation(), Orientation.NORTH);
		tondeuse.turn('D');
		Assert.assertEquals(tondeuse.getOrientation(), Orientation.EAST);
		tondeuse.turn('G');
		Assert.assertEquals(tondeuse.getOrientation(), Orientation.NORTH);
		tondeuse.turn('G');
		Assert.assertEquals(tondeuse.getOrientation(), Orientation.WEST);
		tondeuse.turn('G');
		Assert.assertEquals(tondeuse.getOrientation(), Orientation.SOUTH);
		tondeuse.turn('G');
		Assert.assertEquals(tondeuse.getOrientation(), Orientation.EAST);
	}
	
	@Test
	public void moveForwardTest() {
		tondeuse.moveForward(100,100);
		Assert.assertEquals(1,tondeuse.getX());
		Assert.assertEquals(0,tondeuse.getY());
		tondeuse.moveForward(100,100);
		Assert.assertEquals(2,tondeuse.getX());
		Assert.assertEquals(0,tondeuse.getY());
		tondeuse.setOrientation(Orientation.SOUTH);
		tondeuse.moveForward(100,100);
		Assert.assertEquals(2,tondeuse.getX());
		Assert.assertEquals(0,tondeuse.getY());
		tondeuse.setOrientation(Orientation.WEST);
		tondeuse.moveForward(100,100);
		Assert.assertEquals(1,tondeuse.getX());
		Assert.assertEquals(0,tondeuse.getY());
		tondeuse.setOrientation(Orientation.NORTH);
		tondeuse.moveForward(100,100);
		Assert.assertEquals(1,tondeuse.getX());
		Assert.assertEquals(1,tondeuse.getY());
		tondeuse.setOrientation(Orientation.EAST);
		tondeuse.moveForward(100,100);
		Assert.assertEquals(2,tondeuse.getX());
		Assert.assertEquals(1,tondeuse.getY());
	}
	
	@Test
	public void testResolveInstructions() {
		tondeuse.setX(1);
		tondeuse.setY(2);
		tondeuse.setOrientation(Orientation.NORTH);
		tondeuse.setInstructions("GAGAGAGAA");
		Pelouse pelouse = new Pelouse();
		pelouse.setHeight(5);
		pelouse.setWidth(5);
		tondeuse.resolveInstructions(pelouse);
		Assert.assertEquals(1, tondeuse.getX());
		Assert.assertEquals(3, tondeuse.getY());
		Assert.assertEquals(Orientation.NORTH, tondeuse.getOrientation());
	}
	
	
}
