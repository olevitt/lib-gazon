package com.mowitnow.libgazon.core;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

import org.junit.Assert;
import org.junit.Test;

import com.mowitnow.libgazon.io.OpenPelouseLoader;
import com.mowitnow.libgazon.io.OpenPelouseLoader.LoadingException;


public class PelouseTest {

	@Test
	public void mowTest() throws LoadingException, UnsupportedEncodingException {
		String input = "5 5"+System.lineSeparator()
		+"1 2 N"+System.lineSeparator()
		+"GAGAGAGAA"+System.lineSeparator()
		+"3 3 E"+System.lineSeparator()
		+"AADAADADDA";

		InputStream stream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8.name()));

		Pelouse pelouse = new OpenPelouseLoader().loadPelouse(stream);
		pelouse.mow();

		Assert.assertEquals(1,pelouse.getTondeuses().get(0).getX());
		Assert.assertEquals(3,pelouse.getTondeuses().get(0).getY());
		Assert.assertEquals(Orientation.NORTH,pelouse.getTondeuses().get(0).getOrientation());

		Assert.assertEquals(5,pelouse.getTondeuses().get(1).getX());
		Assert.assertEquals(1,pelouse.getTondeuses().get(1).getY());
		Assert.assertEquals(Orientation.EAST,pelouse.getTondeuses().get(1).getOrientation());
	}
}
