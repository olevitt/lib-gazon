package com.mowitnow.libgazon.io;

import java.io.InputStream;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.mowitnow.libgazon.core.Pelouse;
import com.mowitnow.libgazon.core.Tondeuse;
import com.mowitnow.libgazon.io.OpenPelouseLoader.LoadingException;

public class OpenPelouseLoaderTest {
	
	PelouseLoader pelouseLoader;
	
	@Before
	public void before() {
		pelouseLoader = new OpenPelouseLoader();
	}

	@Test
	public void testLoadPelouse() throws LoadingException {
		InputStream input = OpenPelouseLoader.class.getResourceAsStream("pelouseLoaderTest.txt");
		
		Pelouse pelouse = pelouseLoader.loadPelouse(input);
		
		List<Tondeuse> tondeuses = pelouse.getTondeuses();
		Assert.assertEquals(2, tondeuses.size());
		Assert.assertEquals('N', tondeuses.get(0).getOrientation().getOrientation());
		Assert.assertEquals(1, tondeuses.get(0).getX());
		Assert.assertEquals(2, tondeuses.get(0).getY());
		
		Assert.assertEquals('E', tondeuses.get(1).getOrientation().getOrientation());
		Assert.assertEquals(3, tondeuses.get(1).getX());
		Assert.assertEquals(3, tondeuses.get(1).getY());
		
		Assert.assertEquals("GAGAGAGAA", tondeuses.get(0).getInstructions());
		Assert.assertEquals("AADAADADDA", tondeuses.get(1).getInstructions());
	}
}
